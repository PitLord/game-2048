#!/bin/bash

apt-get update
apt-get install apt-transport-https ca-certificates curl software-properties-common ansible-lint yamllint python3-pip libssl-dev rsync git curl ansible -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt install docker-ce -y
sudo usermod -aG docker vagrant
ansible-galaxy collection install community.crypto ansible.posix community.general
ansible-galaxy collection install community.docker --force
pip install --upgrade pip && sudo pip install pytest-testinfra && sudo pip install "molecule[docker]" && sudo pip install molecule-docker
# Вывод сообщения об успешном выполнении скрипта
echo "Provisioning script executed successfully."

